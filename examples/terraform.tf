variable "aws_region"            { default = "us-east-1" }
variable "name"                  { default = "hstack-test" }
variable "bastion_count"         { default = "1" }
variable "consul_servers_count"  { default = "1" }
variable "hstack_version"        { default = "7" }
variable "consul_encrypt_key"    { }
variable "keypair_name"          { }

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_iam_policy" "consul_discovery" {
    name = "${var.name}-consul-discovery"
    path = "/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1468377974000",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
    EOF
}

resource "aws_iam_role" "consul" {
    name = "${var.name}-consul"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "consul_discovery" {
    name = "${var.name}-consul-discovery"
    roles = ["${aws_iam_role.consul.name}"]
    policy_arn = "${aws_iam_policy.consul_discovery.arn}"
}

resource "aws_iam_instance_profile" "consul" {
    name = "${var.name}-consul"
    role = "${aws_iam_role.consul.name}"
}

module "network" {
   source = "git::https://gitlab.com/hstack/tf-mod-aws-network.git"

   name = "${var.name}-net"

   region = "${var.aws_region}"
   bastion_count = "${var.bastion_count}"
   keypair_name = "${var.keypair_name}"
   hstack_version = "${var.hstack_version}"
   nat_count = "1"
}

module "consul" {
  source = "git::https://gitlab.com/hstack/tf-mod-aws-consul.git"

  name = "${var.name}-consul"

  hstack_version = "${var.hstack_version}"
  keypair_name = "${var.keypair_name}"
  region = "${var.aws_region}"
  iam_instance_profile = "${aws_iam_instance_profile.consul.name}"

  domain = "consul"
  dc = "test"

  subnet_ids = "${module.network.private_subnet_ids}"
  count = "${var.consul_servers_count}"

  encrypt_key = "${var.consul_encrypt_key}"

  autojoin_tag_value = "${var.name}_consulserversjoin"
}

module "openvpn" {
  source             = "git::https://gitlab.com/hstack/tf-mod-aws-openvpn.git"

  name               = "${var.name}-openvpn"
  region             = "${var.aws_region}"
  hstack_version     = "${var.hstack_version}"
  vpc_id             = "${module.network.vpc_id}"
  public_subnet_ids  = "${module.network.public_subnet_ids}"
  private_subnet_ids = "${module.network.private_subnet_ids}"
  keypair_name       = "${var.keypair_name}"
  instance_type      = "t2.micro"

  iam_instance_profile = "${var.name}-consul"
  domain = "consul"
  dc = "test"
  encrypt_key = "${var.consul_encrypt_key}"
  autojoin_tag_value = "${var.name}_consulserversjoin"
}

output "configuration" {
  value = <<CONFIGURATION

* Add your private key and SSH into any private node via the Bastion host:
  $$ ssh-add path_to_private_sshkey
  $$ ssh -A core@${module.network.bastion_public_dns}

* Add these lines to your ~/.ssh/config file to connect to your instances through the bastion:
   ---
   Host "${replace(replace(module.network.vpc_cidr,".0",""),"/\\/[\\d]+/",".*")}"
      IdentityFile path_to_private_sshkey
      User core
      ProxyCommand ssh -l core -i path_to_private_sshkey -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null "${module.network.bastion_public_dns}" ncat %h %p
   ---
CONFIGURATION
}
