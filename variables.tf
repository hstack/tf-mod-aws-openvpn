variable "name"                  { default = "hstack-openvpn" }
variable "atlas_username"        { default = "hstack" }
variable "region"                { default = "us-east-1" }
variable "hstack_version"        { default = "1.0" }
variable "vpc_id"                { }
variable "vpn_cidr"              { default = "192.168.255.0/24"}
variable "public_subnet_ids"     { }
variable "private_subnet_ids"    { }
variable "keypair_name"          { }
variable "instance_type"         { default = "t2.micro" }
variable "push_dns_servers"      { default = "" }
variable "count"                 { default = 1 }

variable "domain"               { default = "consul" }
variable "dc"                   { default = "dc1" }

variable "iam_instance_profile"  { default = "consul" }
variable "encrypt_key"           { }
variable "join_ipv4_addr"        { default = "" }

variable "autojoin_tag_value"   { default = "" }
variable "autojoin_tag_key"     { default = "consul_autojoin" }
