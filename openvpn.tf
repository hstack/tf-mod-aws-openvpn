#--------------------------------------------------------------
# This module creates all resources necessary for OpenVPN
#--------------------------------------------------------------

data "atlas_artifact" "openvpn_ami" {
  name = "${var.atlas_username}/aws-${var.region}-coreos-hstack"

  type = "amazon.image"
  metadata {
     version = "${var.hstack_version}"
  }
}

data "aws_vpc" "selected" {
  id = "${var.vpc_id}"
}


## todo use EFS volume to share openvpn config accross
## instances
#
# resource "aws_efs_file_system" "openvpn" {
#   performanceMode = "generalPurpose"
#   tags {
#     Name = "${var.name}"
#   }
# }
#
# resource "aws_efs_mount_target" "openvpn" {
#   count          = "${var.count)}"
#   file_system_id = "${aws_efs_file_system.openvpn.id}"
#   subnet_id      = "${element(split(",", var.subnet_ids), count.index)}"
# }

resource "aws_security_group" "openvpn" {
  name   = "${var.name}"
  vpc_id = "${var.vpc_id}"
  description = "OpenVPN security group"

  tags { Name = "${var.name}" }

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["${data.aws_vpc.selected.cidr_block}"]
  }

  ingress {
    protocol    = "udp"
    from_port   = 1194
    to_port     = 1194
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "template_file" "openvpn_user_data" {
  template = "${file("${path.module}/openvpn.tpl")}"

  vars {
    private_network     = "${data.aws_vpc.selected.cidr_block}"
    vpn_network         = "${var.vpn_cidr}"
    push_dns_servers    = "${var.push_dns_servers}"
    public_addr         = "${aws_elb.openvpn.dns_name}"

    join_ipv4_addr        = "${var.join_ipv4_addr}"
    autojoin_tag_key      = "${var.autojoin_tag_key}"
    autojoin_tag_value    = "${coalesce(var.autojoin_tag_value, var.name)}"
    domain                = "${var.domain}"
    dc                    = "${var.dc}"
    encrypt_key           = "${var.encrypt_key}"
  }
}

resource "aws_launch_configuration" "openvpn" {
  name_prefix          = "${var.name}"
  image_id             = "${lookup(data.atlas_artifact.openvpn_ami.metadata_full, format("region-%s", var.region))}"
  instance_type        = "${var.instance_type}"
  security_groups      = ["${aws_security_group.openvpn.id}"]
  iam_instance_profile = "${var.iam_instance_profile}"

  user_data     = "${data.template_file.openvpn_user_data.rendered}"

  associate_public_ip_address = "false"
  key_name                = "${var.keypair_name}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "openvpn" {
  name_prefix               = "${var.name}"
  vpc_zone_identifier       = [ "${split(",",var.private_subnet_ids)}"]
  desired_capacity          = "1"
  min_size                  = "1"
  max_size                  = "1"
  health_check_grace_period = "60"
  health_check_type         = "EC2"
  force_delete              = false
  wait_for_capacity_timeout = 0
  launch_configuration      = "${aws_launch_configuration.openvpn.name}"
  enabled_metrics           = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances"
  ]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_elb" "openvpn" {
  name = "${var.name}"

  subnets = ["${split(",", var.public_subnet_ids)}"]

  listener {
    instance_port = 22
    instance_protocol = "tcp"
    lb_port = 22
    lb_protocol = "tcp"
  }


  listener {
    instance_port = 1194
    instance_protocol = "tcp"
    lb_port = 1194
    lb_protocol = "tcp"
  }

  security_groups      = ["${aws_security_group.openvpn.id}"]

  cross_zone_load_balancing = true
  idle_timeout = 400
  connection_draining = true
  connection_draining_timeout = 400

  tags {
    Name = "${var.name}"
  }
}

resource "aws_autoscaling_attachment" "asg_attachment_openvpn" {
  autoscaling_group_name = "${aws_autoscaling_group.openvpn.id}"
  elb                    = "${aws_elb.openvpn.id}"
}
