#cloud-config
write_files:
  - path: "/etc/hstack/hstack.conf"
    permissions: "0644"
    owner: "root"
    content: |
      PRIVATE_NETWORK="${ private_network }"
      OVPN_MODE="server"
      OVPN_DNS="${push_dns_servers}"
      OVPN_ROUTES="${ private_network }"
      OVPN_SERVER="${ vpn_network }"
      OVPN_PUBLIC_ADDRESS="${ public_addr }"
      CONSUL_MODE="agent"
      CONSUL_AUTOJOIN="aws"
      CONSUL_AUTOJOIN_AWS_TAG_VALUE="${ autojoin_tag_value }"
      CONSUL_AUTOJOIN_AWS_TAG_KEY="${ autojoin_tag_key }"
      CONSUL_AGENT_TAGS="consul-server,${autojoin_tag_value}"
      DOMAIN=${ domain }
      DATACENTER=${ dc }
      CONSUL_ENCRYPT_KEY=${ encrypt_key }
      JOIN_IPV4_ADDR="${ join_ipv4_addr }"
